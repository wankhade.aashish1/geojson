package com.example.pocrarnd;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.data.Feature;
import com.google.maps.android.data.Layer;
import com.google.maps.android.data.Point;
import com.google.maps.android.data.geojson.GeoJsonFeature;
import com.google.maps.android.data.geojson.GeoJsonLayer;
import com.google.maps.android.data.geojson.GeoJsonLineString;
import com.google.maps.android.data.geojson.GeoJsonLineStringStyle;
import com.google.maps.android.data.geojson.GeoJsonMultiPolygon;
import com.google.maps.android.data.geojson.GeoJsonPoint;
import com.google.maps.android.data.geojson.GeoJsonPointStyle;
import com.google.maps.android.data.geojson.GeoJsonPolygon;
import com.google.maps.android.data.geojson.GeoJsonPolygonStyle;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    RequestQueue requestQueue;
    Button fetch, drawpolygon, findpolygon, distance;
    GeoJsonLayer layer, wells;
    GoogleMap gmap;
    GeoJsonFeature editFeature;
    LatLng last;
    List<GeoJsonLineString> lines = new ArrayList<>();
    List<LatLng> latLngList = new ArrayList<>();
    List<Marker> markerList = new ArrayList<>();
    private static final String fine_location = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String course_location = Manifest.permission.ACCESS_COARSE_LOCATION;
    private Boolean permissionGranted = false;
    LocationManager locationManager;
    List<LatLng> wellpoints;
    int zInd = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Cache cache = new DiskBasedCache(getCacheDir(), 10*1024 * 1024); // 10MB cap
        Network network = new BasicNetwork(new HurlStack());
        requestQueue = new RequestQueue(cache, network);
        requestQueue.start();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        getLocationPermissions();
        if(editFeature!=null)
            Log.d("feature", editFeature.toString());
        fetch = findViewById(R.id.bt_fetch);
        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(MainActivity.this,"fetching...",Toast.LENGTH_LONG).show();
                fetchJSON();

            }
        });
        drawpolygon = findViewById(R.id.bt_draw);
        drawpolygon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(latLngList.size()!=0){
                    PolygonOptions polygonOptions = new PolygonOptions().addAll(latLngList).clickable(true);
                    List<List<LatLng>> polypoints = new ArrayList<>();
                    polypoints.add(polygonOptions.getPoints());
                    GeoJsonPolygon gp = new GeoJsonPolygon(polypoints);
                    GeoJsonPolygonStyle polygonStyle = new GeoJsonPolygonStyle();
                    polygonStyle.setFillColor(Color.YELLOW);
                    polygonStyle.setZIndex(1);
                    polygonStyle.setStrokeColor(Color.RED);
                    GeoJsonFeature polyfeat = new GeoJsonFeature(gp,null,null,null);
                    polyfeat.setPolygonStyle(polygonStyle);
                    layer.addFeature(polyfeat);
                    latLngList.clear();
                    for(Marker m: markerList){
                        m.remove();
                    }

                }
            }
        });
        findpolygon = findViewById(R.id.bt_findpoly);
        findpolygon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findpoint();
            }
        });
        distance = findViewById(R.id.bt_sync);
        distance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(latLngList.size()>=2){
                    LatLng first = latLngList.get(latLngList.size()-1);
                    LatLng second = latLngList.get(latLngList.size()-2);
                    latLngList.remove(latLngList.size()-1);
                    latLngList.remove(latLngList.size()-1);
                    Marker m = markerList.get(markerList.size()-1);
                    m.remove();
                    markerList.remove(markerList.size()-1);
                    m = markerList.get(markerList.size()-1);
                    m.remove();
                    markerList.remove(markerList.size()-1);

                    Point src = new Point(first);
                    Point dest = new Point(second);
                    Location src1 = new Location("src");
                    src1.setLatitude(first.latitude);
                    src1.setLongitude(first.longitude);
                    Location dest1 = new Location("dest");
                    dest1.setLatitude(second.latitude);
                    dest1.setLongitude(second.longitude);
                    double distance = src1.distanceTo(dest1);
//                    Toast.makeText(MainActivity.this,"Distance : "+String.valueOf(distance) +" m", Toast.LENGTH_LONG).show();
                    openDialog3(distance);
                }
            }
        });
    }
    public void openDialog3(double distance){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Distance: "+distance+" m");
        alertDialog.setMessage("\n");
        final AlertDialog alert = alertDialog.create();
        alert.show();

    }
    public GeoJsonFeature editFeat(){
        return editFeature;
    }
    void fetchJSON(){
        Log.d("f","fetching...");
        String url = "https://www.cse.iitb.ac.in/~ashishw/kharif.geojson";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(gmap!=null) {
                            layer = new GeoJsonLayer(gmap, response);
                            GeoJsonPolygonStyle style = layer.getDefaultPolygonStyle();
                            style.setZIndex(1);
                            layer.setOnFeatureClickListener(new GeoJsonLayer.OnFeatureClickListener() {
                                @Override
                                public void onFeatureClick(Feature feature) {
                                    openDialog(feature);
                                }
                            });
                            layer.addLayerToMap();
                            LatLng maharashtra = new LatLng(19.753130595762087,76.876050561417728);
                            gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(maharashtra,12));
                        }else{
                            Log.d("f","map not initialized");
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Toast.makeText(MainActivity.this,"Failed", Toast.LENGTH_LONG).show();
                    }
        });
        String url1 = "https://www.cse.iitb.ac.in/~ashishw/wells.geojson";
        JsonObjectRequest jsonObjectRequest1 = new JsonObjectRequest
                (Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(gmap!=null) {
                            wells = new GeoJsonLayer(gmap, response);
                            GeoJsonPolygonStyle style = wells.getDefaultPolygonStyle();
                            GeoJsonPointStyle pstyle = wells.getDefaultPointStyle();

                            wells.addLayerToMap();
                            wellpoints = new ArrayList<>();


                            for(GeoJsonFeature gf: wells.getFeatures()){
                                if(gf.hasGeometry() && gf.getGeometry().getGeometryType().equals("Point")){
                                    GeoJsonPoint gp = (GeoJsonPoint) gf.getGeometry();
                                    if(gp!=null){
                                        LatLng p = gp.getCoordinates();
                                        if(p!=null){
                                            Log.d("points", gf.getProperties().toString());
                                            if(gp.getCoordinates() !=null){
                                                wellpoints.add(p);
                                                IconGenerator iconFactory = new IconGenerator(getApplicationContext());
                                                GeoJsonPointStyle p_style = new GeoJsonPointStyle();
//                                                p_style.setTitle();
                                                p_style.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon("  Well\nId: "+gf.getProperty("id")+"\nSurvey No.: "+ gf.getProperty("Syrvey_No"))));
                                                gf.setPointStyle(p_style);
                                            }

                                        }

                                    }
                                }
                            }
                        }else{
                            Log.d("f","map not initialized");
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Toast.makeText(MainActivity.this,"Failed", Toast.LENGTH_LONG).show();
                    }
                });
        requestQueue.add(jsonObjectRequest);
        requestQueue.add(jsonObjectRequest1);
    }


    private void openDialog(final Feature feature){

        Log.d("i", feature.toString());
        editFeature = (GeoJsonFeature) feature;
        GeoJsonPolygonStyle polygonStyle = new GeoJsonPolygonStyle();
        Log.d("color",String.valueOf(editFeature.getPolygonStyle().getFillColor()));
        if(editFeature.getPolygonStyle().getFillColor()==Color.YELLOW){
            polygonStyle.setStrokeColor(Color.BLACK);
            polygonStyle.setFillColor(Color.TRANSPARENT);
        }else{
            Log.d("color","inside");
            polygonStyle.setStrokeColor(Color.RED);
            polygonStyle.setFillColor(Color.YELLOW);
        }

        editFeature.setPolygonStyle(polygonStyle);
        int well_count=0;
        if(feature.hasGeometry() && feature.getGeometry().getGeometryType().equals("MultiPolygon")){
            GeoJsonMultiPolygon gf = (GeoJsonMultiPolygon) feature.getGeometry();
            for(GeoJsonPolygon gp: gf.getPolygons()){
                for(LatLng point: wellpoints){
                    boolean contains = isPointInPolygon(point,gp.getCoordinates().get(0));
                    if(contains) {
                        well_count++;
                    }
                }

            }
        }
        else if(feature.hasGeometry() && feature.getGeometry().getGeometryType().equals("Polygon")){
            GeoJsonPolygon gp = (GeoJsonPolygon) feature.getGeometry();
            for(LatLng point: wellpoints){
                boolean contains = isPointInPolygon(point,gp.getCoordinates().get(0));
                if(contains) {
//                Toast.makeText(getApplicationContext(), "Found", Toast.LENGTH_LONG).show();
//                openDialog(feat);
                    well_count++;
                }
            }


        }
        Toast.makeText(MainActivity.this,"Well count="+well_count,Toast.LENGTH_LONG).show();
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        ScrollView scrollView = new ScrollView(this);
        scrollView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        TextView tv_wells = new TextView(this);
        tv_wells.setPadding(10,0,0,0);
        tv_wells.setTextColor(Color.RED);
        tv_wells.setText("No. of Wells :"+String.valueOf(well_count));
        tv_wells.setTextSize(15);
        linearLayout.addView(tv_wells);

        final Map<String, EditText> map = new HashMap();
        for(String key:feature.getPropertyKeys()){
            TextView tv = new TextView(this);
            tv.setPadding(10,0,0,0);
            tv.setTextColor(Color.RED);
            tv.setText(key+" :");
            linearLayout.addView(tv);
            Object val = feature.getProperty(key);
            if(val!=null){
                EditText et = new EditText(this);
                et.setText(val.toString());
                linearLayout.addView(et);
                map.put(key,et);
            }else {
                EditText et = new EditText(this);
                et.setText("null");
                linearLayout.addView(et);
                map.put(key,et);
            }
        }
        Button addProps = new Button(this);
        addProps.setText("Add Properties");
        addProps.setPadding(10,0,0,0);
        linearLayout.addView(addProps);
        Button save = new Button(this);
        save.setText("Save Locally");
        save.setPadding(10,0,0,0);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(String key:feature.getPropertyKeys()){
                    editFeature.setProperty(key,map.get(key).getText().toString());
                }
                Toast.makeText(getApplicationContext(),"Saved Successfully", Toast.LENGTH_LONG).show();
            }
        });
        linearLayout.addView(save);
        scrollView.addView(linearLayout);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Properties");
        alertDialog.setView(scrollView);
        final AlertDialog alert = alertDialog.create();
        alert.show();
        addProps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                Toast.makeText(getApplicationContext(),"Open new dialog",Toast.LENGTH_LONG).show();
                openDialog1(feature);

            }
        });
    }
    private void openDialog1(final Feature feature){
        editFeature = (GeoJsonFeature) feature;
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        ScrollView scrollView = new ScrollView(this);
        scrollView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        LinearLayout keyll = new LinearLayout(this);
        keyll.setOrientation(LinearLayout.HORIZONTAL);
        keyll.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        TextView keytv = new TextView(this);
        keytv.setText("Key: ");
        keytv.setPadding(15,0,0,0);
        keytv.setTextColor(Color.BLACK);
        keyll.addView(keytv);
        final EditText keyet = new EditText(this);
        keyet.setMinimumWidth(1000);
        keyll.addView(keyet);
        linearLayout.addView(keyll);

        LinearLayout valll = new LinearLayout(this);
        valll.setOrientation(LinearLayout.HORIZONTAL);
        valll.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        TextView valtv = new TextView(this);
        valtv.setText("Value: ");
        valtv.setPadding(15,0,0,0);
        valtv.setTextColor(Color.BLACK);
        valll.addView(valtv);
        final EditText valet = new EditText(this);
        valet.setMinimumWidth(1000);
        valll.addView(valet);
        linearLayout.addView(valll);

        scrollView.addView(linearLayout);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Add Properties");
        alertDialog.setView(scrollView);
        alertDialog.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String key = keyet.getText().toString();
                String value = valet.getText().toString();
                if(key.trim().length()!=0 && value.trim().length()!=0) {
                    editFeature.setProperty(key, value);
                    Toast.makeText(getApplicationContext(),"Added Successfully!!!",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_LONG).show();
                }
            }
        });
        final AlertDialog alert = alertDialog.create();
        alert.show();
    }

    private void findpoint(){
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        ScrollView scrollView = new ScrollView(this);
        scrollView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        LinearLayout keyll = new LinearLayout(this);
        keyll.setOrientation(LinearLayout.HORIZONTAL);
        keyll.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        TextView keytv = new TextView(this);
        keytv.setText("Latitude: ");
        keytv.setPadding(15,0,0,0);
        keytv.setTextColor(Color.BLACK);
        keyll.addView(keytv);
        final EditText keyet = new EditText(this);
        keyet.setMinimumWidth(9000);
        keyll.addView(keyet);
        linearLayout.addView(keyll);

        LinearLayout valll = new LinearLayout(this);
        valll.setOrientation(LinearLayout.HORIZONTAL);
        valll.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        TextView valtv = new TextView(this);
        valtv.setText("Longitude: ");
        valtv.setPadding(15,0,0,0);
        valtv.setTextColor(Color.BLACK);
        valll.addView(valtv);
        final EditText valet = new EditText(this);
        valet.setMinimumWidth(9000);
        valll.addView(valet);
        linearLayout.addView(valll);

        scrollView.addView(linearLayout);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Find Polygon");
        alertDialog.setView(scrollView);
        alertDialog.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String lat = keyet.getText().toString();
                String longi = valet.getText().toString();
                if(lat.trim().length()!=0 && longi.trim().length()!=0){
                    try{
                        Double lat_d = Double.parseDouble(lat);
                        Double long_d = Double.parseDouble(longi);
                        LatLng point = new LatLng(lat_d,long_d);
                        for(GeoJsonFeature feat:layer.getFeatures()){
                            if(feat.hasGeometry() && feat.getGeometry().getGeometryType().equals("MultiPolygon")){
                                GeoJsonMultiPolygon gf = (GeoJsonMultiPolygon) feat.getGeometry();
                                for(GeoJsonPolygon gp: gf.getPolygons()){

                                    boolean contains = isPointInPolygon(point,gp.getCoordinates().get(0));
                                    if(contains) {
                                        openDialog(feat);
                                    }
                                }
                            }
                            else if(feat.hasGeometry() && feat.getGeometry().getGeometryType().equals("Polygon")){
                                GeoJsonPolygon gp = (GeoJsonPolygon) feat.getGeometry();
                                boolean contains = isPointInPolygon(point,gp.getCoordinates().get(0));
                                if(contains) {
                                    Toast.makeText(getApplicationContext(), "Found", Toast.LENGTH_LONG).show();
                                    openDialog(feat);
                                }

                            }
                        }

                    }catch (Exception e){
                        Toast.makeText(getApplicationContext(),"Failed1", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Failed2", Toast.LENGTH_LONG).show();

                }
            }
        });
        final AlertDialog alert = alertDialog.create();
        alert.show();
    }

    private boolean isPointInPolygon(LatLng tap, List<LatLng> vertices) {
        int intersectCount = 0;
        for (int j = 0; j < vertices.size() - 1; j++) {
            if (rayCastIntersect(tap, vertices.get(j), vertices.get(j + 1))) {
                intersectCount++;
            }
        }

        return ((intersectCount % 2) == 1); // odd = inside, even = outside;
    }

    private boolean rayCastIntersect(LatLng tap, LatLng vertA, LatLng vertB) {

        double aY = vertA.latitude;
        double bY = vertB.latitude;
        double aX = vertA.longitude;
        double bX = vertB.longitude;
        double pY = tap.latitude;
        double pX = tap.longitude;

        if ((aY > pY && bY > pY) || (aY < pY && bY < pY)
                || (aX < pX && bX < pX)) {
            return false; // a and b can't both be above or below pt.y, and a or
            // b must be east of pt.x
        }

        double m = (aY - bY) / (aX - bX); // Rise over run
        double bee = (-aX) * m + aY; // y = mx + b
        double x = (pY - bee) / m; // algebra is neat!

        return x > pX;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap=googleMap;
        gmap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions markerOptions = new MarkerOptions().position(latLng);
                Marker marker = gmap.addMarker(markerOptions);
                ArrayList<LatLng> lineStringArray = new ArrayList<LatLng>();
                if(last != null)
                    lineStringArray.add(last);
                lineStringArray.add(latLng);
                last=latLng;
                GeoJsonLineString lineString = new GeoJsonLineString(lineStringArray);
                lines.add(lineString);
                latLngList.add(latLng);
                markerList.add(marker);
                String text = "Latitude: "+String.valueOf(latLng.latitude)+"\nLongitude: "+String.valueOf(latLng.longitude);
                Toast.makeText(MainActivity.this,"Latitude: "+String.valueOf(latLng.latitude)+"\nLongitude: "+String.valueOf(latLng.longitude), Toast.LENGTH_LONG).show();
                try{
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("label", text);
                    clipboard.setPrimaryClip(clip);
                }catch (Exception e){}
            }
        });

        gmap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                try {
                    Geocoder gc = new Geocoder(MainActivity.this, Locale.getDefault());
                    List<Address> addrs = null;
                    addrs = gc.getFromLocation(latLng.latitude, latLng.longitude, 2);
                    String country =addrs.get(0).getCountryName();
                    zInd++;
                    Marker marker = gmap.addMarker(new MarkerOptions().position(latLng).title(
                            "aloha").zIndex(zInd));

                    ArrayList<LatLng> lineStringArray = new ArrayList<LatLng>();
                    if(last != null)
                        lineStringArray.add(last);
                    lineStringArray.add(latLng);
                    last=latLng;
                    GeoJsonLineString lineString = new GeoJsonLineString(lineStringArray);
                    lines.add(lineString);
                    latLngList.add(latLng);
                    markerList.add(marker);
                    Toast.makeText(MainActivity.this,"Latitude: "+String.valueOf(latLng.latitude)+"\nLongitude: "+String.valueOf(latLng.longitude), Toast.LENGTH_LONG).show();
                    String text = "Latitude: "+String.valueOf(latLng.latitude)+"\nLongitude: "+String.valueOf(latLng.longitude);
                    try{
                        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("label", text);
                        clipboard.setPrimaryClip(clip);
                    }catch (Exception e){}

                }catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    private void getLocationPermissions() {
        Log.d("PErm_MapAct", "Getting location permission");

        String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), fine_location) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(), course_location) == PackageManager.PERMISSION_GRANTED) {
                permissionGranted = true;
                initMap();
            } else {
                ActivityCompat.requestPermissions(this, permission, 1234);
            }
        } else {
            ActivityCompat.requestPermissions(this, permission, 1234);
        }
    }

    private void initMap() {
        Log.d("InitMap", "InitMap");
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) == true){
            Log.e("Create","ifWorked");
            Toast.makeText(MainActivity.this,"Please Enable GPS",Toast.LENGTH_LONG).show();
        }else{
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map);
            mapFragment.getMapAsync(MainActivity.this);

        }
    }
}
