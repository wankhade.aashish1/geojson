package com.example.pocrarnd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.maps.android.data.Feature;
import com.google.maps.android.data.geojson.GeoJsonFeature;

import java.util.HashMap;
import java.util.Map;


public class EditProperty extends AppCompatActivity {
    ScrollView sc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_property);
        Bundle extras = getIntent().getExtras();
        String keys = extras.getString("keys");
        final String keyArr[] = keys.split(",");
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        ScrollView scrollView = new ScrollView(this);
        scrollView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        setContentView(scrollView);
        final Map<String, EditText> map = new HashMap();
        for(String key:keyArr){
            TextView tv = new TextView(this);
            tv.setPadding(10,0,0,0);
            tv.setTextColor(Color.RED);
            tv.setText(key+" :");
            linearLayout.addView(tv);
            Object val = extras.get(key);
            if(val!=null){
                EditText et = new EditText(this);
                et.setText(val.toString());
                linearLayout.addView(et);
                map.put(key,et);
            }else {
                EditText et = new EditText(this);
                et.setText("null");
                linearLayout.addView(et);
                map.put(key,et);
            }
        }
        Button save = new Button(this);
        save.setText("Save Locally");
        save.setPadding(10,0,0,0);
        linearLayout.addView(save);
        scrollView.addView(linearLayout);
        final int feature_id = extras.getInt("feature");
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),String.valueOf(feature_id),Toast.LENGTH_LONG).show();
//                GeoJsonFeature ft = MainActivity.editFeat();
//                Log.d("temp", ft.toString());
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                for(String key:keyArr){
//                    intent.putExtra(key,map.get(key).getText().toString());
//                    ft.setProperty(key,map.get(key).getText().toString());
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

    }
}
